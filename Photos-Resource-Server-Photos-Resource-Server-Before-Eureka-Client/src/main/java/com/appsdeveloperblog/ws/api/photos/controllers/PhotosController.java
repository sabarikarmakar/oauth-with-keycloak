/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appsdeveloperblog.ws.api.photos.controllers;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.appsdeveloperblog.ws.api.photos.response.Photos;

@RestController
@RequestMapping("/photos")
public class PhotosController {

	@GetMapping
	public List<Photos> getPhotos() {

		Photos photo1 = new Photos();
		photo1.setAid("albumIdHere");
		photo1.setPid("1");
		photo1.setUid(1L);
		photo1.setTitle("Photo 1 title");
		photo1.setDescription("Photo 1 description");
		photo1.setUrl("Photo 1 URL");

		Photos photo2 = new Photos();
		photo2.setAid("albumIdHere");
		photo2.setPid("2");
		photo2.setUid(2L);
		photo2.setTitle("Photo 2 title");
		photo2.setDescription("Photo 2 description");
		photo2.setUrl("Photo 2 URL");

		return Arrays.asList(photo1, photo2);
	}

//	@GetMapping("/with/{pid}/and/{aid}")
//	public String getPhoto(@PathVariable String pid,@PathVariable String aid) {
//
//		return photosService.findByPidAndAid(pid,aid);
//		
//	}

}
