/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appsdeveloperblog.ws.api.albums.controllers;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.appsdeveloperblog.ws.api.albums.response.AlbumRest;

@RestController
@RequestMapping("/albums")
public class AlbumsController {
    
    @GetMapping
    public List<AlbumRest> getAlbums() { 
        
        AlbumRest album1 = new AlbumRest();
        album1.setAid("albumIdHere");
        album1.setUid(1L);
        album1.setTitle("Album 1 title");
        album1.setDescription("Album 1 description");
        album1.setUrl("Album 1 URL");
        
        AlbumRest album2 = new AlbumRest();
        album2.setAid("albumIdHere");
        album2.setUid(2L);
        album2.setTitle("Album 2 title");
        album2.setDescription("Album 2 description");
        album2.setUrl("Album 2 URL");
         
        return Arrays.asList(album1, album2);
    }
 
}