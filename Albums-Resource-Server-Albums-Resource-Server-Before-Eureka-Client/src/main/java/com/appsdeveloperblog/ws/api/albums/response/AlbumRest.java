/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appsdeveloperblog.ws.api.albums.response;

public class AlbumRest {
private Long uid;
	
	private String pid;
	private String aid;
	private String title;
	private String description;
	private String url;
	
	public AlbumRest() {
		super();
	}
	
	
	public AlbumRest(Long uid, String pid, String aid, String title, String description, String url) {
		super();
		this.uid = uid;
		this.pid = pid;
		this.aid = aid;
		this.title = title;
		this.description = description;
		this.url = url;
	}
	public Long getUid() {
		return uid;
	}
	public void setUid(Long uid) {
		this.uid = uid;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getAid() {
		return aid;
	}
	public void setAid(String aid) {
		this.aid = aid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
    
    
}
